package ru.levelup.lesson8.util;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class StringUtilsTest {

    // whenValueIsNull_thenReturnTrue
    // testname_input_return
    @Test
//    @Ignore
    public void testIsEmpty_valueIsNull_returnTrue() {
        // given
        String value = null;
        // when
        boolean result = StringUtils.isEmpty(value);
        // then
        // assert result;
        Assert.assertTrue(result);
    }

    @Test
    public void testIsEmpty_valueIsNotEmpty_returnFalse() {
        String value = "value";
        boolean result = StringUtils.isEmpty(value);
        Assert.assertFalse(result);
    }

}
