package ru.levelup.lesson8.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import ru.levelup.lesson7.domain.ApplicationUser;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

// before - перед КАЖДЫМ тестовым методом
// beforeClass - перед выполнением всего теста (тестового класса)
// after - после КАЖДОГО тестового метода
// afterClass - после выполнения всех тестов
@RunWith(MockitoJUnitRunner.class)
public class ApplicationUserServiceImplTest {

    @Mock
    private SessionFactory factory;
    private Session session;
    private Transaction transaction;

    @InjectMocks
    private ApplicationUserServiceImpl service;
    @Before
    public void setup() {
        session = Mockito.mock(Session.class);
        transaction = Mockito.mock(Transaction.class);
        when(factory.openSession()).thenReturn(session);
        when(session.beginTransaction()).thenReturn(transaction);
    }

//    {
//        MockitoAnnotations.initMocks(this);
//    }
    // class A implements SessionFactory {}
    // factory = new A()
    // service = new ApplicationUserServiceImpl(factory)
    @Test
    public void testCreateUser() {
        service.createUser("", "");

//        Mockito.verify(transaction, Mockito.times(2)).commit();
        Mockito.verify(transaction).commit();
        Mockito.verify(session).close();
    }

    @Test
    public void testUpdate_validParams() {
        int id = 10;
        String login = "login";
        String password = "password";
        ApplicationUser user = new ApplicationUser(id, login, password);

        when(session.get(ApplicationUser.class, id))
                .thenReturn(user);

        service.update(id, login, password);
    }

}

