package ru.levelup.lesson8.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.levelup.lesson7.SessionFactoryInitializer;
import ru.levelup.lesson7.domain.ApplicationUser;

public class ApplicationUserServiceImpl implements ApplicationUserService {

    private SessionFactory factory;

    public ApplicationUserServiceImpl(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public void createUser(String login, String password) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        transaction.commit();
        session.close();
    }

    @Override
    public void update(int id, String login, String password) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        ApplicationUser user = session.get(ApplicationUser.class, id);
        user.setUserLogin(login);
        user.setPassword(password);

        transaction.commit();
        session.close();
    }
}
