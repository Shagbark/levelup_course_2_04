package ru.levelup.lesson8.service;

public interface ApplicationUserService {

    void createUser(String login, String password);

    void update(int id, String login, String password);

}
