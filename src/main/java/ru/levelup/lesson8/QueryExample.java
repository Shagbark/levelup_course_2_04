package ru.levelup.lesson8;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.levelup.lesson7.SessionFactoryInitializer;
import ru.levelup.lesson7.domain.ApplicationUser;

import java.util.List;

// Rich domain model
// Anemic domain model
public class QueryExample {

    public static void main(String[] args) {
        SessionFactory factory = SessionFactoryInitializer.getFactory();
        Session session = factory.openSession();

        // HQL - hibernate query language
        List<ApplicationUser> users = session.createQuery(
                "from ApplicationUser where userLogin = :login", ApplicationUser.class)
                .setParameter("login", "login")
                .getResultList();

        System.out.println(users);

        session.close();
        factory.close();
    }

}
