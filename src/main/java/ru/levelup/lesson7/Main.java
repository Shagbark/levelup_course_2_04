package ru.levelup.lesson7;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.levelup.lesson7.domain.ApplicationUser;

public class Main {

    public static void main(String[] args) {
        SessionFactory factory = SessionFactoryInitializer.getFactory();

        ApplicationUser user = new ApplicationUser();
        user.setUserLogin("hibernate_login");
        user.setPassword("password");

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = (Integer) session.save(user);
        System.out.println("Id: " + id);

        user.setPassword("password will be updated");

        transaction.commit();
        session.close();

        user.setPassword("password won't be updated");

        factory.close();
    }

}
