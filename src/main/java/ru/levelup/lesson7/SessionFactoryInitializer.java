package ru.levelup.lesson7;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryInitializer {

    private static SessionFactory factory;

    static {
        Configuration configuration = new Configuration()
                .configure("/lesson7/configuration.cfg.xml");
        factory = configuration.buildSessionFactory();
    }

    public static SessionFactory getFactory() {
        return factory;
    }

}
