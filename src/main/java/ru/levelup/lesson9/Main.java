package ru.levelup.lesson9;

import ru.levelup.lesson7.domain.ApplicationUser;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Counter counter = new Counter();
        new Thread(new RequestRunner(counter)).start();
        new Thread(new RequestRunner(counter)).start();
        new Thread(new RequestRunner(counter)).start();
        new Thread(new RequestRunner(counter)).start();
    }

}
