package ru.levelup.lesson9;

import java.util.Date;

public interface EventCounter {

    void registerEvent();

    void registerEvent(Date date);

}
