package ru.levelup.lesson9;

import ru.levelup.lesson7.domain.ApplicationUser;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

// synchronized
// biased, thin, fat
// CAS - compare and swap - неблокирующая операция для записи значения в память
// biased - один раз CAS для сохранения блокировки у потока (одного)
// thin - каждый раз CAS при взятии блокировки
// fat - когда растет contention переходим в fat.
public class Counter {

    private Long value;
    private volatile int available = 1;
    private Lock lock = new ReentrantLock();

    private final Object mutex = new Object();

    public  long incrementAndGet(){
        while (available == 0) {}
        available = 0;
//        return ++value;
        long v = value;
        available = 1;
        return v;
    }

    public long incrementAndGetWithLock() {
        lock.lock();
        return ++value;
    }

    public void unlock() {
        lock.unlock();
    }

    public long incrementAngGetWithSync() {
        ///
        synchronized (this) {
            //
            return ++value;
        }
        ///
    }

    public long get() {
        return value;
    }

}
