package ru.levelup.lesson9;

public class RequestRunner implements Runnable {

    private Counter counter;

    public RequestRunner(Counter counter) {
        this.counter = counter;
    }

    @Override
    public void run() {
        try {
            while (true) {
                long i = counter.incrementAndGet();
                System.out.println(Thread.currentThread().getName() + " " + i);
                Thread.sleep(400);
            }
        } catch (InterruptedException exc) {
            throw new RuntimeException(exc);
        }
    }

}
