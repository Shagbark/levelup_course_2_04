package ru.levelup.lesson2.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;


public class CrashPhone {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, InvocationTargetException {

        // Object[] find("ru.level.up")

        Phone firstPhone = new Phone("iPhone", 4);
        Phone process = RandomIntProcessor.process(firstPhone);
        System.out.println(process.getCoreNumber());
        Phone processedPhone = RandomIntProcessor.process(Phone.class);

        // 1 способ - вызов метода getClass().
        Class classOfPhone = firstPhone.getClass();

        // 2 способ - вызов через имя класса
        Class literalClassPhone = Phone.class;

        Field[] fields = classOfPhone.getDeclaredFields();
        Arrays.stream(fields).forEach(field -> {
            System.out.println(field.getName() + ", type: " + field.getType().getName());
        });

        Constructor[] constructors = classOfPhone.getDeclaredConstructors();
        Constructor withoutParameters = Arrays.stream(constructors)
                .filter(constructor -> constructor.getParameterCount() == 0)
                .findFirst()
                .get();

        try {
            withoutParameters.setAccessible(true);
            Phone createPhone = (Phone) withoutParameters.newInstance();
            System.out.println(createPhone.toString());
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }

}
