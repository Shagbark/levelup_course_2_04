package ru.levelup.lesson2.reflection;

public class Phone {

    private String name;
    @RandomInt(min = 1, max = 12)
    private int coreNumber;
    @RandomInt(min = 1024, max = 8096)
    private int ram;

    private Phone() {}

    public Phone(String name, int coreNumber) {
        this.name = name;
        this.coreNumber = coreNumber;
    }

    public int getCoreNumber() {
        return coreNumber;
    }

    public int getRam() {
        return ram;
    }
}
