package ru.levelup.lesson10.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "message")
public class Message {

    @Id
    private int id;
    private String text;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private User user;

}
