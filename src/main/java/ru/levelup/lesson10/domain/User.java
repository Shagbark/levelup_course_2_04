package ru.levelup.lesson10.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Setter
@Getter
@Entity
@Table(name = "user")
@NoArgsConstructor
public class User implements Serializable {

    @Id
    private int id;
    private String login;

    @OneToOne(mappedBy = "user", fetch = FetchType.LAZY, cascade = {
            CascadeType.PERSIST,
            CascadeType.REMOVE
    })
    private UserDetails details;

    @OneToMany(mappedBy = "user")
    private Collection<Message> messages;

    @ManyToMany
    @JoinTable(
            name = "users_in_room",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "room_id")
    )
    private Collection<Room> rooms;

}
