package ru.levelup.lesson10.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table(name = "user_info")
public class UserDetails {

    @Id
    @OneToOne
    private User user;

    private int age;

}
