package ru.levelup.lesson10;

import java.util.concurrent.*;

public class ExecutorExample {

    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(10, r -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });
        Executors.newSingleThreadExecutor();
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(100);
        ses.schedule(() -> {}, 100, TimeUnit.MINUTES); // через 100 минут - один раз
        ses.scheduleWithFixedDelay(() -> {}, 0, 100, TimeUnit.SECONDS); // каждый 100 секунд
        ses.scheduleAtFixedRate(() -> {
            System.out.println("esdfs");
        }, 0, 100, TimeUnit.SECONDS);    // между задачами 100 секунд

        ExecutorService executorService = new ThreadPoolExecutor(10, 16, 100, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(15));


    }

}
