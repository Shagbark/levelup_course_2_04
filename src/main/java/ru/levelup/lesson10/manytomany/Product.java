package ru.levelup.lesson10.manytomany;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Setter
@Getter
@Table(name = "product")
@Entity
public class Product {

    @Id
    private int id;
    private String productName;

    @OneToMany
    private Collection<ProductOrder> orders;

}
