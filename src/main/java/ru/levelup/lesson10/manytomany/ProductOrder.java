package ru.levelup.lesson10.manytomany;

import javax.persistence.*;

@Entity
@Table(name = "product_order")
public class ProductOrder {

    @EmbeddedId
    private ProductOrderId id;

    private int quantity;

    @ManyToOne
    @MapsId("productId")
    private Product product;

    @ManyToOne
    @MapsId("orderId")
    private Order order;


}
