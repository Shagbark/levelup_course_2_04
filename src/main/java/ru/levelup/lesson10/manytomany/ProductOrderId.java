package ru.levelup.lesson10.manytomany;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ProductOrderId implements Serializable {

    private int productId;
    private int orderId;

}
