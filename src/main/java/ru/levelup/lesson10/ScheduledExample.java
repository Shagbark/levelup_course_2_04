package ru.levelup.lesson10;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledExample {

    public static void main(String[] args) {
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(10);
        ses.scheduleAtFixedRate(() -> {
            System.out.println("Inoke");
        }, 2, 3, TimeUnit.SECONDS);
    }

}
