package ru.levelup.lesson10;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DatesExample {

    public static void main(String[] args) {
        LocalDateTime time = LocalDateTime.of(2019, 6, 4, 19, 0, 0);
        ZonedDateTime utc = time.atZone(ZoneId.of("UTC+3"));
        System.out.println(utc.toString());
        ZonedDateTime inUtc = utc.withZoneSameInstant(ZoneId.of("UTC"));
        System.out.println(inUtc);

        long l = time.atZone(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();
        System.out.println(l);

    }

}
