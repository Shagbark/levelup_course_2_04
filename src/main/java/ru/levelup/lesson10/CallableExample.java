package ru.levelup.lesson10;

import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.*;

public class CallableExample {

    @SneakyThrows
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(4);

        Future<Double> future = executorService.submit(() -> {
            double value = 369345;
            return Math.sqrt(value * value / ((value + value / 87) / 8));
        });
        // future.cancel(false);
        future.get(100, TimeUnit.SECONDS);

        int[] array = new int[100_000_000];
        ExecutorService executor = Executors.newFixedThreadPool(4);
        Collection<Future<Integer>> results = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            Future<Integer> result = executor.submit(new MaxFinder(25_000_000, i, array));
            // 0 - 24 999 999
                // 25 000 000 - 49 999 999
                // 50 000 000 - 74 999 999
                // 75 000 000 - 99 999 999
//                int part = 100_000_000 / 4;
//                int left = part * i;
//                int right =
            // result.get()
            results.add(result);
        }
        for (Future<Integer> result : results) {
            System.out.println(result.get());
        }
    }

    private static class MaxFinder implements Callable<Integer> {

        int left;
        int right;
        int[] array;

        MaxFinder(int part, int index, int[] array) {
            this.left = part * index;
            this.right = left + part - 1;
            this.array = array;
        }

        @Override
        public Integer call() throws Exception {
            int max = array[left];
            for (int i = left; i <= right; i++ ) {
                if (max < array[i]) {
                    max = array[i];
                }
            }
            return max;
        }

    }

}
