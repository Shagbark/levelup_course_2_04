package ru.levelup.lesson1;

public class ThreadExample {

    // Thread
    // Runnable
    // Callable

    public static void main(String[] args) {
        Thread thread = new Thread();
        thread.start();

        Thread myThread = new MyThread();
        myThread.setDaemon(true);
        myThread.start();

        for (int i = 0; i < 10; i++) {
            System.out.println(i * i);
        }

        Thread fromRunnable = new Thread(new MyRunnable());
        fromRunnable.start();
//        fromRunnable.stop();
    }

    public static class MyThread extends Thread {

        @Override
        public void run() {
            System.out.println("Before sleep");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Message from MyThread");
        }

    }

    public static class MyRunnable implements Runnable {

        @Override
        public void run() {
            System.out.println("Program will finish in 5 seconds");
            for (int i = 5; i > 0; i--) {
                System.out.println(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
