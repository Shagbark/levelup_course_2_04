insert into users_info values
                              (1, 'First', '2019-05-21', 'first@example.com', '+79259867874'),
                              (2, 'Petr', '2019-05-20', 'petr@mail.ru', '+79568542359'),
                              (3, 'Ivan', '2019-04-29', 'ivan@yandex.ru', '+79635891274');

select * from users_info;

insert into users values (6, 'test8', 'test');
insert into users(login, password) values ('test5', 'test2');
select * from users;

insert into rooms(name) values
                               ('Dev'),
                               ('Flood'),
                               ('C++'),
                               ('Java');

/** 1, 2 -> 1*/
/* 1, 2, 3 -> 2 */
/* 2, 3 -> 4 */

insert into users_in_rooms values
                                  (1, 1),
                                  (1, 2),
                                  (2, 1),
                                  (2, 2),
                                  (2, 4),
                                  (3, 2),
                                  (3, 4);

insert into messages(author_id, room_id, date, text) values
                                                            (1, 1, '2019-04-05', 'Hello'),
                                                            (1, 2, '2019-04-05', 'World!');

select * from users, users_info;

select * from users, users_info where id = user_id;
/* select * from users, users_info where users.id = users_info.user_id;
select * from users u, users_info ui where u.id = ui.user_id; */

select * from users
                inner join users_info on id = user_id
                inner join users_in_rooms uir on id = uir.user_id;

select * from users
                left outer join users_info on id = user_id;