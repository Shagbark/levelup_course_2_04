create table if not exists users (
  id serial primary key,
  login varchar(50) not null unique,
  password varchar(100) not null
);

create table if not exists user_info (
  id bigint primary key,
  name varchar(50) not null,
  last_name varchar(50) not null,
  last_login timestamp
);

create table if not exists rooms (
  id serial primary key,
  name varchar(50) not null unique
);

create table if not exists message(
  id serial primary key,
  author_id bigint not null,
  room_id bigint not null,
  text text not null,
  date timestamp not null,
  foreign key (author_id) references users(id),
  foreign key (room_id) references rooms(id)
);

create table if not exists users_in_room(
  user_id bigint not null,
  room_id bigint not null,
  field int,
  primary key (room_id, user_id),
  foreign key (room_id) references rooms(id),
  foreign key (user_id) references users(id)
);